/**
 * Form
 * form.js
 */
class Form {
  setInputData(data) {
    Modal.show();
    $.ajax({
      url: API_URL + 'set-input-data',
      method: 'POST',
      data: data.serialize()
    }).done((res) => {
      Modal.hide();
      toastButton('Data berhasil diperbaharui', 'ok');
    });
  }
}

const formData = new Form();

const saveData = () => {
  const data = $('#form-data');
  formData.setInputData(data)
}