/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * My App Config
 */
const Navigator = document.getElementById('myNavigator');
const Modal = $('ons-modal');

const API_URL = 'http://mental.w3b-project.com/api/';
const KEY = '';

/**
 * Check Session
 */

let isLogin = localStorage.getItem('isLogin');
let username = localStorage.getItem('user_id');

const checkSess = () => {
  if (isLogin === false || username === "" || username === null || isLogin.length === 0 || username.length === 0) {
    Navigator.pushPage('pages/login.html');
  } else {
    Navigator.pushPage('pages/home.html');
  }
}
ons.ready(() => {
  checkSess();
});
/**
 * Toast
 */
const toastButton = (msg, labelButton) => {
  ons.notification.toast(msg, {
    buttonLabel: labelButton,
    animation: 'ascend',
    timeout: 3000,
    force: true
  });
}
/**
 * Init Page
 */
document.addEventListener('init', (e)=> {
  let page = e.target;
  if (page.id === "menu") {
    listMenu();
  } else if (page.id === "form-input") {
    $('#form-input #title').html(page.data.title);
    $('#form-input input[name="lembaga_id"]').val(localStorage.getItem('lembaga_id'));
    $('#form-input input[name="master_data_id"]').val(page.data.master_id);
  } else if (page.id === "profile") {
    // loadSelect();
  }
}, false);