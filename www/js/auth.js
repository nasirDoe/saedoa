class Auth {

  processLogin(username, password) {
    let status = true;

    if (username === "" || password === "") {
      toastButton('Username dan atau Password kosong', 'Tutup');
      status = false;
    }

    if(status) {
      Modal.show();
      $.ajax({
        url: API_URL + 'login',
        method: 'POST',
        dataType: 'json',
        crossDomain: true,
        data: {
          username: username,
          password: password
        }
      }).done((res) => {
        Modal.hide();
        const user = res;
        if(user.status == "success") {
          Navigator.replacePage('pages/home.html')
          .then(() => {
            localStorage.setItem("user_id", user.userdata.user_id);
            localStorage.setItem("lembaga_id", user.userdata.lembaga_id);
            localStorage.setItem("isLogin", true);
            toastButton('Selamat datang '+ user.userdata.name, 'ok');
          });
        } else {
          toastButton(user.message, 'Tutup');
        }
      }).fail((e) => {
        alert(JSON.stringify(e));
      });
    }
  }

  processLogout() {
    ons.notification.confirm({
      message: 'Keluar aplikasi ?',
      cancelLabels: true,
      buttonLabels: ['Ya', 'Batal'],
      callback: function(idx) {
        switch (idx) {
          case 0:
            localStorage.setItem('user_id', "");
            localStorage.setItem('isLogin', false);
            localStorage.clear();
            myNavigator.replacePage('pages/login.html');
          break;
          case 1:
          break;
        }
      }
    });
  }

  getProfile(id) {
    // this.getKabkot();
    // this.getDistrik();
    Navigator.pushPage('pages/profile.html')
    .then(() => {
      Modal.show();
      $.ajax({
        url:API_URL + 'get-lembaga',
        method: 'POST',
        data: {
          id: id
        }
      }).done((res) => {
        Modal.hide();
        let html = `<ons-list-header>
        Informasi lembaga
      </ons-list-header>
      <ons-list-item>
        <ons-row>
          <ons-col width="100%" class="mb-3 pt-3">
            <ons-input name="nama" value="`+ res.nama+`" modifier="material" float placeholder="Nama lembaga" class="w-100"></ons-input>
          </ons-col>
          <ons-col width="100%" class="mb-3 pt-3">
            <ons-input name="npwp" value="`+ res.npwp +`" modifier="material" float placeholder="NPWP" class="w-100"></ons-input>
          </ons-col>
          <ons-col width="100%" class="mb-3 pt-3">
            <ons-input value="`+ res.tahun_berdiri +`" modifier="material" float placeholder="Tahun berdiri" class="w-100"></ons-input>
          </ons-col>
          <ons-col width="100%" class="mb-3 pt-3">
            <ons-input value="`+ res.no_sk +`" modifier="material" float placeholder="No. SK" class="w-100"></ons-input>
          </ons-col>
          <ons-col width="100%" class="pt-3">
            <ons-input value="`+ res.tgl_sk +`" modifier="material" float placeholder="Tgl. SK" class="w-100"></ons-input>
          </ons-col>
        </ons-row>
      </ons-list-item>`;

      html += `<ons-list-header>
        Informasi alamat
      </ons-list-header>
      <ons-list-item>
        <ons-row>
          <ons-col width="100%" class="mb-3 pt-3">
            <ons-input value="`+ res.alamat +`" modifier="material" float placeholder="Alamat" class="w-100"></ons-input>
          </ons-col>
          <input type="hidden" name="kabkota_id" value="`+ res.kabkota_id +`">
          <input type="hidden" name="distrik_id" value="`+ res.distrik_id +`">
          <input type="hidden" name="kelurahan_id" value="`+ res.kelurahan_id +`">
        </ons-row>
      </ons-list-item>`;
      // <ons-col width="100%" class="mb-3 pt-3">
      //   <ons-select class="w-100">
      //     <select id="kabkota" name="kabkota_id"></select>
      //   </ons-select>
      // </ons-col>
      // <ons-col width="100%" class="mb-3 pt-3">
      //   <ons-select class="w-100">
      //     <select id="distrik"></select>
      //   </ons-select>
      // </ons-col>
      // <ons-col width="100%" class="pt-3">
      //   <ons-select class="w-100" id="kelurahan">
      //   </ons-select>
      // </ons-col>
      html +=`<ons-list-header>Informasi kontak</ons-list-header>
      <ons-list-item>
        <ons-row>
          <ons-col width="100%" class="mb-3 pt-3">
            <ons-input value="`+ res.no_telepon +`" modifier="material" float placeholder="No. Telepon" class="w-100"></ons-input>
          </ons-col>
          <ons-col width="100%" class="mb-3 pt-3">
            <ons-input value="`+ res.email +`" modifier="material" float placeholder="Email" class="w-100"></ons-input>
          </ons-col>
          <ons-col width="100%" class="mb-3 pt-3">
            <ons-input value="`+ res.website +`" modifier="material" float placeholder="Website" class="w-100"></ons-input>
          </ons-col>
          <ons-col width="100%" class="mb-3 pt-3">
            <ons-input value="`+ res.kode_pos +`" modifier="material" float placeholder="Kode pos" class="w-100"></ons-input>
          </ons-col>
        </ons-row>
      </ons-list-item>`
      $('#profile #profile-data').html(html);
      });
    });
  }
  getKabkot() {
    $.ajax({
      url: API_URL + 'get-kabkota',
      method: 'POST',
      success: function(res) {
        $.each(res, function(i, row) {
          $("#profile #kabkota").append('<option value='+ i +'>' + row + '</option>');
        });
      }
    });
  }
  getDistrik() {
    $('#profile #kabkota').on('change', function() {
      alert('boom')
      let id = $(this).find('option:selected').val();
      $.ajax({
        url: API_URL + 'get-distrik',
        method: 'POST',
        data: {
          kabkota_id: id
        },
        success: function (res) {
          $.each(res,function(i, value) {
            $("#profile #distrik").append('<option value=' + i + '>' + value + '</option>');
          });
        }
      })
    });
  }
}

const authClass = new Auth();

const doLogin = () => {
  let user = $('#login #username').val();
  let pass = $('#login #password').val();
  authClass.processLogin(user, pass);
}

const doLogout = () => {
  authClass.processLogout();
}

const profile = () => {
  let id = localStorage.getItem('lembaga_id');
  authClass.getProfile(id);
}