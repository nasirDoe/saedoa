/**
 * Menu
 * menu.js
 */
const listMenu = () => {
  Modal.show();
  $.ajax({
    url: API_URL + 'get-master-data',
    method: 'POST',
    crossDomain: true,
  }).done((res) => {
    Modal.hide();
    let html = '';
    $.each(res, (i, row)=> {
      html += '<ons-list-item modifier="material chevron" tappable onclick="toForm(\'' + row + '\', '+ i +')">'+ row +'</ons-list-item>';
      $('#menu #list-menu').html(html);
    });
  });
}
/**
 * To Form
 */
const toForm = (formName, id) => {
  Navigator.pushPage('pages/form-input.html', {
    data: {
      title: formName,
      master_id: id
    }
  })
  .then(() => {
    Modal.show();
    $.ajax({
      url: API_URL + 'get-input-data',
      method: 'POST',
      data: {
        lembaga_id: localStorage.getItem('lembaga_id'),
        master_data_id: id
      }
    }).done((res) => {
      Modal.hide();
      const data = res.data;
      let html = '';
      $.each(data, (i, row) => {
        html +=`<ons-col width="100%" class="mb-3 pt-3">
                  <ons-input class="w-100" name="input[` +row.id+ `]" value="`+ row.nilai +`" float placeholder="`+ row.nama +`"></ons-input>
                </ons-col>`;
        $('#form-input #form-field').html(html);
      });
    });
  });
}